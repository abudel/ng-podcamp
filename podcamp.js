(function() {
    "use strict";
  
    angular.module("ngPodcamp", []).provider("podcamp", PodcampProvider);
  
    function PodcampProvider() {
        this.options = {
            apiHost: "/api",
            defaultCultureId: 1033
        };
  
        /** @ngInject */
        this.$get = function($http, $q, $window, $filter) {
            return new PodcampService($http, $q, $window, $filter, this.options);
        };
    }
    /** @ngInject */
    function PodcampService($http, $q, $window, $filter, options) {
        var apiHost = options.apiHost;
        var defaultCultureId = options.defaultCultureId;
        var storageMode = "sessionStorage";
        var canUseStorage = isStorageEnabled();
  
        var config = {
            headers: {
                "Content-Type": "application/json"
            }
        };
        if (!angular.isUndefined(options.headers)) {
            angular.extend(config.headers, options.headers);
        }
  
        return {
            getCultures: getCultures,
            getVehicleTypes: getVehicleTypes,
            getProductCategoryDetail: getProductCategoryDetail,
            getProductCategoryMedias: getProductCategoryMedias,
            getProductCategoryServices: getProductCategoryServices,
            getSetupInfo: getSetupInfo,
            getProductCategoriesSetupInfo: getProductCategoriesSetupInfo,
            getFacilities: getFacilities,
            getReservationProposal: getReservationProposal,
            getDolReservation: getDolReservation,
            setReservation: setReservation,
            getProductCategoryAddons: getProductCategoryAddons,
            setReservationAddons: setReservationAddons,
            getReservationBill: getReservationBill,
            getReservationBillAddons: getReservationBillAddons,
            addReservationPayment: addReservationPayment,
            setReservationStatus: setReservationStatus,
            getCategoryTypeFromProductCategory: getCategoryTypeFromProductCategory,
            getCountries: getCountries,
            getPlaces: getPlaces,
            addOwner: addOwner,
            getOwner: getOwner,
            setOwner: setOwner,
            login: login,
            resetPin: resetPin,
            checkToken: checkToken,
            getOwnerComponents: getOwnerComponents,
            setPin: setPin,
            getAgeRanges: getAgeRanges,
            getReservations: getReservations,
            getReservation: getReservation,
            saveReservation: saveReservation,
            getContactTypes: getContactTypes,
            getIdentityDocumentTypes: getIdentityDocumentTypes,
            getIssuingAuthorities: getIssuingAuthorities,
            encodeCulture: encodeCulture,
            decodeCulture: decodeCulture,
            getReservationCriteria: getReservationCriteria,
            stripeFacilityIsEnabled: stripeFacilityIsEnabled,
            getStripeCustomerCards: getStripeCustomerCards,
            getStripeClient: getStripeClient,
            stripeAddCustomer: stripeAddCustomer,
            stripeAddCharge: stripeAddCharge,
            getPass: getPass,
            clientCompletion: clientCompletion,
            parseReservation: parseReservation,
            decodeSearchCriteria: decodeSearchCriteria,
            encodeSearchCriteria: encodeSearchCriteria,
            savePreCheckin: savePreCheckin,
            getPreCheckin: getPreCheckin,
            stripeAddCustomerPreCheckin: stripeAddCustomerPreCheckin,
            saveReservationVehicles: saveReservationVehicles,
            getAdultAgeRange: getAdultAgeRange,
            // Bol in richiesta
            getCategoryQuestions: getCategoryQuestions,
            saveCategoryAnswers: saveCategoryAnswers,
            getProductCategoryMode: getProductCategoryMode,
            isProductCategoryInRequestMode: isProductCategoryInRequestMode,
            // Push notifications
            subscribePushNotification: subscribePushNotification,
            unsubscribePushNotification: unsubscribePushNotification,
            getInterestsPushNotification: getInterestsPushNotification,
            saveInterestsPushNotification: saveInterestsPushNotification,
            getUserInterestsPushNotification: getUserInterestsPushNotification,
            updateTokenPushNotification: updateTokenPushNotification,
            getHistoryPushNotifications: getHistoryPushNotifications,
            // Where are u
            getStayByPass: getStayByPass,
            getCategoriesForStay: getCategoriesForStay,
            getProductsForStay: getProductsForStay,
            updateProductStay: updateProductStay
        };
  
        /**
       * controllo se è possibile scrivere nel local/sessions storage
       * @NOTE safari in private mode non consente di scrivere dati
       * @return {Boolean} [description]
       */
        function isStorageEnabled() {
            try {
                if (storageMode === "sessionStorage") {
                    $window.sessionStorage.setItem("_test", 1);
                    $window.sessionStorage.removeItem("_test");
                } else if (storageMode === "localStorage") {
                    $window.Storage._test = 1;
                    delete $window.Storage._test;
                } else {
                    return false;
                }
                return true;
            } catch (e) {
                return false;
            }
        }
  
        /**
       * Culture disponibili per la facility
       * @param  {[type]} facilityId [description]
       * @return {[type]}            [description]
       */
        function getCultures(facilityId) {
            var url = apiHost + "/v1/facility/cultures/" + facilityId;
            return $http.get(url, config).then(function(response) {
                return response.data;
            });
        }
        /**
         * Tièpologie veicoli
         */
        function getVehicleTypes(cultureId){
            var url = apiHost + "/v1/products/vehicle-types?cultureId="+cultureId;
            return $http.get(url, config).then(function(response){
                return response.data;
            });
        }
        /**
       * Dettaglio del productCategory
       * @param  {[type]} productCategoryId [description]
       * @param  {[type]} cultureId         [description]
       * @return {[type]}                   [description]
       */
        function getProductCategoryDetail(productCategoryId, cultureId) {
            var url = apiHost + "/v1/products/category/" + productCategoryId + "?cultureId=" + (cultureId || defaultCultureId);
            return $http.get(url, config).then(function(response) {
                return response.data;
            });
        }
        /**
       * Media associati al product category
       * @param  {[type]} productCategoryId [description]
       * @return {[type]}                   [description]
       */
        function getProductCategoryMedias(productCategoryId, cultureId) {
            var url = apiHost + "/v1/products/category/media/" + productCategoryId + "?cultureId=" + (cultureId || defaultCultureId);
            return $http.get(url, config).then(function(response) {
                return response.data;
            });
        }
        /**
       * Servici abbinati al productCateogry
       * @param  {[type]} productCategoryId [description]
       * @param  {[type]} facilityId        [description]
       * @param  {[type]} cultureId         [description]
       * @return {[type]}                   [description]
       */
        function getProductCategoryServices(productCategoryId, facilityId, cultureId) {
            var url = apiHost + "/v1/products/category/services/" + productCategoryId + "?facilityId=" + facilityId + "&cultureId=" + (cultureId || defaultCultureId);
            return $http.get(url, config).then(function(response) {
                return response.data;
            });
        }
  
        /**
       * Recupera le informazioni per la organizzazione
       * specificata.
       * Serve per configurare il selectore di risorse
       * @param  {Number} organizationId Id della organizzazione
       * @param  {Number} cultureId      cultura 
       * @return {Promises}              
       */
        function getSetupInfo(organizationId, cultureId) {
            if (!cultureId) {
                cultureId = defaultCultureId;
            }
            var url = apiHost + "/v1/reservations/setup-info?organizationId=" + organizationId + "&cultureId=" + cultureId;
            return $http.get(url, config).then(function(response) {
                // salvo i dati recuperati nei cookies
                // memSet(cacheName, response.data);
                return response.data;
            });
        }
  
        /**
       * Recupera le informazioni per la organizzazione
       * specificata senza il controllo dei periodi di prenotabilità
       * Serve per configurare il selectore di risorse
       * @param  {Number} organizationId Id della organizzazione
       * @param  {Number} cultureId      cultura 
       * @return {Promises}              
       */
        function getProductCategoriesSetupInfo(organizationId, cultureId) {
            if (!cultureId) {
                cultureId = defaultCultureId;
            }
            var url = apiHost + "/v1/reservations/productCategory-info?organizationId=" + organizationId + "&cultureId=" + cultureId;
            /*
        RIMUOVO LA CACHE DALLA SESSIONE
        var cacheName = 'productCategory-info_'+organizationId+'_'+cultureId;
        var response = memGet(cacheName);
        if(response){
          var deferred = $q.defer();
          deferred.resolve(response);
          return deferred.promise;
        }
        */
            return $http.get(url, config).then(function(response) {
                // salvo i dati recuperati nei cookies
                // memSet(cacheName, response.data);
                return response.data;
            });
        }
  
        /** semplice local storage per alcune chiamate */
        function memSet(name, value) {
            if (canUseStorage) {
                $window.sessionStorage.setItem(name, JSON.stringify(value));
            }
        }
        function memGet(name) {
            if (canUseStorage) {
                var value = $window.sessionStorage.getItem(name);
                if (value && value !== "") {
                    return JSON.parse(value);
                }
            }
            return false;
        }
  
        /** 
       * Recupera la categoryType
       * di un productCategory
       */
        function getCategoryTypeFromProductCategory(organizationId, facilityId, productCategoryId, cultureId) {
            return getSetupInfo(organizationId, cultureId).then(function(response) {
                var facility, categoryType;
                var i, j;
                for (i = 0; i < response.facilitySetupInfo.length; i++) {
                    if (response.facilitySetupInfo[i].facilityId === facilityId) {
                        facility = response.facilitySetupInfo[i];
                        break;
                    }
                }
                if (!facility) {
                    return null;
                }
                for (i = 0; i < facility.categoryTypes.length; i++) {
                    for (j = 0; j < facility.categoryTypes[i].categoriesSetupInfo.length; j++) {
                        if (facility.categoryTypes[i].categoriesSetupInfo[j].productCategoryId === parseInt(productCategoryId, 10)) {
                            categoryType = facility.categoryTypes[i];
                            break;
                        }
                    }
                    if (categoryType) {
                        break;
                    }
                }
                return categoryType;
            });
        }
  
        /**
       * Ricerca delle risorse per il criterio di ricerca specificato
       * @param  {CriteriaObject} criteria    Criterio di ricerca
       * @param  {String} search      Tipo di ricerca *'auto', 'future', 'past'*
       * @param  {Number} pricelistId Id del listino
       * @param  {Number} cultureId   Id della cultura
       * @param  {Boolean} isChecked   se sono stati controllati i vincoli della risorsa
       * @return {Promises}             
       */
        function getReservationProposal(criteria, search, pricelistId, cultureId, isChecked, maxAttempts) {
            var url = apiHost + "/v1/reservations/new-proposals";
            // var url = apiHost + "/v1/reservations/test-proposals";
            return $http
                .post(
                    url,
                    {
                        criteria: criteria,
                        search: search || "auto",
                        pricelistId: pricelistId || null,
                        cultureId: cultureId || defaultCultureId,
                        isChecked: isChecked || false,
                        maxAttempts: maxAttempts ? maxAttempts : !search || search === "auto" || search === "Auto" ? 3 : 3 // massimo numeri di salti nella ricerca disponibilità
                    },
                    config
                )
                .then(function(response) {
                    return response.data;
                });
        }
  
        /**
       * Recupra la prenotazione effettuata via DOL ( Diocanaia On Line )
       * @param  {String} email         email di contatto
       * @param  {Number} reservationId Id prenotazione
       * @param  {Number} cultureId     Id cultura
       * @return {Promises}               
       */
        function getDolReservation(email, reservationId, cultureId) {
            var url = apiHost + "/v1/login/dol";
            return $http
                .post(
                    url,
                    {
                        reservationId: reservationId,
                        username: email,
                        cultureId: cultureId || defaultCultureId
                    },
                    config
                )
                .then(function(response) {
                    return response.data;
                });
        }
  
        /**
       * recupera la prenotazione 
       * @param  {Number} reservationId Id prenotazione
       * @param  {Number} cultureId     Id cultura
       * @return {Promises}                 Oggetto Prenotazione
       */
        function getReservation(reservationId, cultureId) {
            var url = apiHost + "/v1/reservations/detail/" + reservationId + "?cultureId=" + cultureId || defaultCultureId;
            return $http.get(url, config).then(function(response) {
                return response.data;
            });
        }
  
        /**
       * Salvo la prenotazione 
       * @param  {CriteriaObject} criteria  Criteria di ricerca
       * @param  {ProposalObject} proposal  Proposta di risorsa
       * @param  {Array}          people    lista di persone che faranno parte della vacanza Note: Basta passare solo l'owner della vacanza 
       * @param  {String}         notes     Note inseribili dall'utente sulla vacanza
       * @return {Promises}                 Oggetto Prenotazione
       */
        function setReservation(criteria, proposal, people, notes) {
            var url = apiHost + "/v1/reservations";
            return $http
                .post(
                    url,
                    {
                        criteria: criteria,
                        proposal: proposal,
                        people: people,
                        notes: notes || ""
                    },
                    config
                )
                .then(function(response) {
                    return response.data;
                });
        }
  
        /**
       * Recupera gli Addons per una determintata productCategory
       * @param  {[type]} facilityId        [description]
       * @param  {[type]} productCategoryId [description]
       * @param  {[type]} from              [description]
       * @param  {[type]} to                [description]
       * @param  {[type]} cultureId         [description]
       * @return {[type]}                   [description]
       */
        function getProductCategoryAddons(facilityId, productCategoryId, from, to, cultureId) {
            cultureId = cultureId || defaultCultureId;
            var url = apiHost + "/v1/products/category/relationships?productCategoryId=" + productCategoryId + "&from=" + from + "&to=" + to + "&cultureId=" + cultureId + "&facilityId=" + facilityId;
            return $http.get(url, config).then(function(response) {
                return response.data;
            });
        }
  
        /**
       * Salva gli addons alla prenotazione
       * @param {[type]} reservationId [description]
       * @param {[type]} relationships [description]
       */
        function setReservationAddons(reservationId, added, removed, updated, cultureId) {
            var url = apiHost + "/v1/reservations/relationships";
            return $http
                .post(
                    url,
                    {
                        reservationId: reservationId,
                        addedReservationItemRelationships: added,
                        deletedReservationItemRelationships: removed,
                        updatedReservationItemRelationships: updated,
                        cultureId: cultureId
                    },
                    config
                )
                .then(function(response) {
                    return response.data;
                });
        }
  
        /**
       * Recupero il conto della prenotazione
       * @param  {Number} vacationId Id della vacation
       * @return {Promises}               
       */
        function getReservationBill(vacationId, discountCode) {
            var url = apiHost + "/v1/reservations/" + vacationId + "/bill";
            if (discountCode) {
                url += "?discountCode=" + discountCode;
            }
            return $http.get(url, config).then(function(response) {
                return response.data;
            });
        }
  
        /**
       * Recupero il conto della prenotazione
       * virtuale passandone gli addons
       * @param  {Number} reservationId Id della prenotazione
       * @return {Promises}               
       */
        function getReservationBillAddons(reservationId, added, removed, updated, cultureId) {
            var url = apiHost + "/v1/billdetails/virtual";
  
            return $http
                .post(
                    url,
                    {
                        reservationId: reservationId,
                        addedReservationItemRelationships: added,
                        deletedReservationItemRelationships: removed,
                        updatedReservationItemRelationships: updated,
                        cultureId: cultureId
                    },
                    config
                )
                .then(function(response) {
                    return response.data;
                });
        }
  
        /**
       * Aggiungo un metodo di pagamento
       * @param {Number} reservationId        Id prenotazione
       * @param {PaymentObject} payment       Oggetto di pagamento
       */
        function addReservationPayment(reservationId, payment) {
            var url = apiHost + "/v1/reservations/" + reservationId + "/payments";
            return $http.put(url, payment, config).then(function(response) {
                return response.data;
            });
        }
  
        // SPRITE **/
      
      function getStripeClient(token){
          var url = apiHost + "/v1/stripe/get-client?token="+encodeURIComponent(token)
          return $http.get(url, config).then(function(response) {
              return response.data;
          });
      }
      
        /**
       * Ritorna se la facility ha abilitata stripe
       * @param  {[type]} facilityId [description]
       * @return {[type]}            [description]
       */
        function stripeFacilityIsEnabled(facilityId) {
            var url = apiHost + "/v1/stripe/check-facility?facilityId=" + facilityId;
            var myConfig = angular.extend(
                {
                    transformResponse: [
                        function(data) {
                            return data === "True" || data === "true";
                        }
                    ]
                },
                config
            );
            return $http.get(url, myConfig).then(function(response) {
                return response.data;
            });
        }
        /**
       * Ritorna le carte usate dal customer stripe
       * @param  {[type]} clientId [description]
       * @return {[type]}          [description]
       */
        function getStripeCustomerCards(clientId, facilityId) {
            var url = apiHost + "/v1/stripe/check-client?clientId=" + clientId + "&facilityId=" + facilityId;
            return $http.get(url, config).then(function(response) {
                return response.data;
            });
        }
  
        /**
       * Aggiunge un cliente a stripe
       * @return {[type]} [description]
       */
        /**
       * [stripeAddCustomer description]
       * @param  {[type]} facilityId [description]
       * @param  {[type]} cultureId  [description]
       * @param  {[type]} client
       */
        function stripeAddCustomer(token, cardHolderName, cardHolderEmail, clientId, facilityId, cultureId) {
            var url = apiHost + "/v1/stripe/add-customer";
            return $http
                .post(url, {
                    facilityId: facilityId,
                    clientId: clientId,
                    cultureId: cultureId || defaultCultureId,
                    cardHolderName: cardHolderName,
                    cardHolderEmail: cardHolderEmail,
                    token: token
                })
                .then(function(response) {
                    return response.data;
                });
        }
        // Amount, Description, ClientId, FacilityId, VacationId, ReservationId
  
        function stripeAddCharge(client, reservation, amount, description) {
            var url = apiHost + "/v1/stripe/add-charge";
            return $http
                .post(url, {
                    amount: amount,
                    reservationId: reservation.reservationId,
                    vacationId: reservation.vacationId,
                    description: description,
                    facilityId: reservation.facilityId,
                    clientId: client.clientId
                })
                .then(function(response) {
                    return response.data;
                });
        }
  
        /**
       * Modifica lo stato della prenotazione 
       * 1 Opzione
       * 2 Confermata
       * 3 Richiesta di deposito
       * 4 Disdetta
       * 5 Pagamento di deposito avvenuto
       * 6 Inizializzata
       * 7 Richiesta di pre-deposito
       * 8 Pagamento di pre-deposito avvenuto
       * 9 Conferma pre-deposito
       * 
       * @param {Number}                  reservationId Id Prenotazione
       * @param {ReservationStatusObject} state        stato delle prenotazione
       */
        function setReservationStatus(reservationId, state, cultureId) {
            var url = apiHost + "/v1/reservations/" + reservationId + "/state";
            return $http
                .post(
                    url,
                    {
                        state: state,
                        cultureId: cultureId || defaultCultureId
                    },
                    config
                )
                .then(function(response) {
                    return response.data;
                });
        }
  
        /**
       * Recupera le country
       * @return {Promises} 
       */
        function getCountries(cultureId) {
            var url = apiHost + "/v1/reservations/countries?cultureId=" + (cultureId || defaultCultureId);
            return $http.get(url, config).then(function(response) {
                return response.data;
            });
        }
  
        /**
       * Recupera le città di una nazione
       * @param  {String} nane      nome da filtrare
       * @param  {Number} countryId Id Nazione
       * @return {Promises}           Lista delle città
       */
        function getPlaces(name, countryId, cultureId) {
            var url = apiHost + "/v1/reservations/" + countryId + "/search?searchTerm=" + name + "&cultureId=" + (cultureId || defaultCultureId);
            return $http.get(url, config).then(function(response) {
                return response.data;
            });
        }
  
        /**
       * Controlla se gia esiste un cliente
       * e offre la possibilità di forzare la scrittura delle informazioni
       * nel caso ci sia, con il parametro force
       * @param  {ClientObject} client     Oggetto Client
       * @param  {Number}       facilityId Id facility
       * @param  {Number}       cultureId  Id cultura
       * @param  {Boolean}      force      forza la sovrascrittura dei dati
       * @return {Promises}            
       */
        function addOwner(client, facilityId, cultureId, force) {
            var url = apiHost + "/v1/reservations/check-client";
            return $http
                .post(
                    url,
                    {
                        client: client,
                        facilityId: facilityId,
                        cultureId: cultureId || defaultCultureId,
                        force: force || false
                    },
                    config
                )
                .then(function(response) {
                    return response.data;
                });
        }
  
        /**
       * Recupero l'ggetto client
       * @return {[type]} [description]
       */
        function getOwner(clientId, cultureId) {
            var url = apiHost + "/v1/reservedarea/get-owner?clientId=" + clientId + "&cultureId=" + (cultureId || defaultCultureId);
            return $http.get(url, config).then(function(response) {
                return response.data;
            });
        }
  
        /**
       * salva il client nella anagrafica 
       * @param  {[type]} clientId  [description]
       * @param  {[type]} cultureId [description]
       * @return {[type]}           [description]
       */
        function setOwner(client, facilityId, cultureId) {
            var url = apiHost + "/v1/reservedarea/save-client?facilityId=" + facilityId + "&cultureId=" + (cultureId || defaultCultureId);
            return $http.put(url, client, config).then(function(response) {
                return response.data;
            });
        }
  
        /**
       * Effettua il login tramite le credenziali
       * @param  {String} username   Email o PASS
       * @param  {String} password   password
       * @param  {Number} facilityId Id Facility
       * @param  {Number} cultureId  Id Cultura
       * @return {Promises}          
       */
        function login(username, password, facilityId, cultureId) {
            var url = apiHost + "/v1/login/bol";
            return $http
                .post(
                    url,
                    {
                        username: username,
                        pin: password,
                        facilityId: facilityId,
                        cultureId: cultureId || defaultCultureId
                    },
                    config
                )
                .then(function(response) {
                    return response.data;
                });
        }
  
        /**
       * Resetta la password/pin tramite mandrillApp
       * Il cliente riceverà una mail con un link che userà
       * per reimpostare la password
       * @param  {String} username      email
       * @param  {Number} clientId      Id Cliente
       * @param  {Number} facilityId    Id Facility 
       * @param  {String} refId         Id di riferimento customizzato
       * @param  {String} redirectUrl   url sul quale si verra reindirizzati
       * @param  {Number} cultureId     Id cultura
       * @return {[type]}               [description]
       */
        function resetPin(username, clientId, facilityId, redirectUrl, refId, cultureId) {
            var url = apiHost + "/v1/login/resetpin";
            return $http
                .post(
                    url,
                    {
                        username: username,
                        clientId: clientId,
                        facilityId: facilityId,
                        url: redirectUrl,
                        bolId: refId || "",
                        cultureId: cultureId || defaultCultureId
                    },
                    config
                )
                .then(function(response) {
                    return response.data;
                });
        }
  
        /**
       * Quando viene richiesta il reset password, nel redirect
       * viene passato anche un token.
       * Il token deve essere controllato se è ancora valido o meno.  
       * @param  {String} token 
       * @return {Promises}       
       */
        function checkToken(token) {
            // controllo se devo fare un replace degli spazi
            token = encodeURIComponent(token);
            var url = apiHost + "/v1/login/resetpin?token=" + token;
            return $http.get(url, config).then(function(response) {
                return response.data;
            });
        }
  
        /**
       * Recupera i componenti che sono andati in vacanza 
       * con l'owner.
       * NB: E' valido solo per i proprietari, coloro che hanno effetuato la prenotazione.
       * 
       * @param  {Number} clientId  Id Cliente
       * @param  {String} firstName      
       * @param  {String} lastName   
       * @param  {Number} cultureId 
       * @return {Promises}           
       */
        function getOwnerComponents(clientId, firstName, lastName, cultureId) {
            if (!cultureId) {
                cultureId = defaultCultureId;
            }
            var url = apiHost + "/v1/reservedarea/get-component?clientId=" + clientId + "&firstname=" + firstName + "&lastname=" + lastName + "&cultureId=" + cultureId;
            return $http.get(url, config).then(function(response) {
                return response.data;
            });
        }
  
        /**
       * Setta il pin per l'utente
       * @param {Number} clientId 
       * @param {String} username      
       * @param {String} pin      
       */
        function setPin(clientId, username, pin) {
            var url = apiHost + "/v1/login/setpin";
            return $http
                .post(
                    url,
                    {
                        clientId: clientId,
                        pin: pin,
                        username: username
                    },
                    config
                )
                .then(function(response) {
                    return response.data;
                });
        }
  
        /**
       * Recupera gli intervalli di età per i componenti
       * @param  {Number} facilityId Id facility
       * @param  {Number} cultureId  Id Cultura
       * @return {Promises}            
       */
        function getAgeRanges(facilityId, cultureId) {
            if (!cultureId) {
                cultureId = defaultCultureId;
            }
            var url = apiHost + "/v1/reservedarea/get-age-ranges?facilityId=" + facilityId + "&cultureId=" + cultureId;
            return $http.get(url, config).then(function(response) {
                return response.data;
            });
        }
  
        /**
       * recupera le prenotazioni eseguite da un determinato
       * utente
       * @param  {Number} facilityId    Id facility
       * @param  {Number} cultureId     Id cultura
       * @param  {Number} clientId      Id cliente
       * @param  {Number} reservationId (*) Id prenotazione per recuperarne il dettaglio
       * @return {Promises}       
       */
        function getReservations(facilityId, cultureId, clientId, reservationId) {
            if (!cultureId) {
                cultureId = defaultCultureId;
            }
            var url = apiHost + "/v1/reservedarea/get-reservations?facilityId=" + facilityId + "&cultureId=" + cultureId + "&clientId=" + clientId;
            if (reservationId) {
                url += "&reservationId=" + reservationId;
            }
            return $http.get(url, config).then(function(response) {
                return response.data;
            });
        }
  
        function saveReservation(reservation, facilityId) {
            var url = apiHost + "/v1/reservedarea/save-reservation-data";
  
            return $http.put(url, reservation, config).then(function(response) {
                return response.data;
            });
        }
  
        /**
       * Tipologie di contatto
       * @param  {[type]} facilityId [description]
       * @param  {[type]} cultureId  [description]
       * @return {[type]}            [description]
       */
        function getContactTypes(facilityId, cultureId) {
            if (!cultureId) {
                cultureId = defaultCultureId;
            }
            var url = apiHost + "/v1/reservedarea/get-contact-types?facilityId=" + facilityId + "&cultureId=" + cultureId;
            return $http.get(url, config).then(function(response) {
                return response.data;
            });
        }
  
        /**
       * Tipologie di documento
       * @param  {[type]} facilityId [description]
       * @param  {[type]} cultureId  [description]
       * @return {[type]}            [description]
       */
        function getIdentityDocumentTypes(facilityId, cultureId) {
            if (!cultureId) {
                cultureId = defaultCultureId;
            }
            var url = apiHost + "/v1/reservedarea/get-identity-documents-types?facilityId=" + facilityId + "&cultureId=" + cultureId;
            return $http.get(url, config).then(function(response) {
                return response.data;
            });
        }
  
        /**
       * Tipologie di autorità per i documenti
       * @param  {[type]} facilityId [description]
       * @param  {[type]} cultureId  [description]
       * @return {[type]}            [description]
       */
        function getIssuingAuthorities(facilityId, cultureId) {
            if (!cultureId) {
                cultureId = defaultCultureId;
            }
            var url = apiHost + "/v1/reservedarea/get-issuing-authorities?facilityId=" + facilityId + "&cultureId=" + cultureId;
            return $http.get(url, config).then(function(response) {
                return response.data;
            });
        }
  
        /**
       * recupera l'immagine del pass
       * @param  {[type]} clientId 
       * @return {[type]} [description]
       */
        function getPass(clientId) {
            var customConfig = {
                headers: {
                    "Content-Type": "image/png"
                }
            };
            var url = apiHost + "/v1/reservedarea/barcode?clientId=" + clientId;
            return $http.get(url, customConfig).then(function(response) {
                return response.data;
            });
        }
  
        /**
       * recupera le domande da visualizzare
       */
        function getCategoryQuestions(productCategoryId, cultureId) {
            if (!cultureId) {
                cultureId = defaultCultureId;
            }
            var url = apiHost + "/v1/reservations/category-questions?productCategoryId=" + productCategoryId + "&cultureId=" + cultureId;
            return $http.get(url, config).then(function(response) {
                return response.data;
            });
        }
        function saveCategoryAnswers(answers) {
            var url = apiHost + "/v1/reservations/category-answers";
            return $http.post(url, answers, config).then(function(response) {
                return response.data;
            });
        }
  
        function saveReservationVehicles(reservationUnit){
            var url = apiHost + "/v1/reservedarea/save-reservation-vehicles";
            return $http.post(url, {
                reservationUnit: reservationUnit              
            }, config).then(function(response) {
                return response.data;
            });
        }
  
        function isProductCategoryInRequestMode(organizationId, productCategoryId, cultureId) {
          return getSetupInfo(organizationId, 1033).then(function(response) {
              var productCategory = null;
              var i, j, h;
              for (i = 0; i < response.facilitySetupInfo.length; i++) {
                  for (j = 0; j < response.facilitySetupInfo[i].categoryTypes.length; j++) {
                      for (h = 0; h < response.facilitySetupInfo[i].categoryTypes[j].categoriesSetupInfo.length; h++) {
                          if (response.facilitySetupInfo[i].categoryTypes[j].categoriesSetupInfo[h].productCategoryId === parseInt(productCategoryId, 10)) {
                              productCategory = response.facilitySetupInfo[i].categoryTypes[j].categoriesSetupInfo[h];
                              break;
                          }
                      }
                      if (productCategory) {
                          break;
                      }
                  }
                  if (productCategory) {
                      break;
                  }
              }
  
              return productCategory ? productCategory.bolMode !== 0 : false;
          });
        }
        /**
       * Controlla se un specifico Prodotto
       * deve essere in richiesta
       */
        function getProductCategoryMode(organizationId, productCategoryId, cultureId) {
            return getSetupInfo(organizationId, 1033).then(function(response) {
                var productCategory = null;
                var i, j, h;
                for (i = 0; i < response.facilitySetupInfo.length; i++) {
                    for (j = 0; j < response.facilitySetupInfo[i].categoryTypes.length; j++) {
                        for (h = 0; h < response.facilitySetupInfo[i].categoryTypes[j].categoriesSetupInfo.length; h++) {
                            if (response.facilitySetupInfo[i].categoryTypes[j].categoriesSetupInfo[h].productCategoryId === parseInt(productCategoryId, 10)) {
                                productCategory = response.facilitySetupInfo[i].categoryTypes[j].categoriesSetupInfo[h];
                                break;
                            }
                        }
                        if (productCategory) {
                            break;
                        }
                    }
                    if (productCategory) {
                        break;
                    }
                }
  
                return productCategory ? productCategory.bolMode : null;
            });
        }
  
        /**
       * Converte la cultura nel codice cultura
       * @param  {String} culture iso cultura
       * @return {Number}         id Cultura
       */
        function encodeCulture(culture) {
            var id;
            switch (culture) {
                case "it_IT":
                case "it-IT":
                case "it":
                    id = 1040;
                    break;
                case "de_DE":
                case "de-DE":
                case "de":
                    id = 1031;
                    break;
                default:
                    id = 1033;
                    break;
            }
            return id;
        }
  
        /**
       * Converte l'id cultura nella cultura
       * @param  {Number} culture id cultura
       * @return {String}         iso Cultura
       */
        function decodeCulture(culture) {
            var string = "en";
            switch (culture) {
                case 1040:
                    string = "it";
                    break;
                case 1031:
                    string = "de";
                    break;
                case 1033:
                    string = "en";
                    break;
            }
            return string;
        }
  
        /**
       * Costruisco l'oogetto criteria per eseguire la ricerca
       *
       * *Criteria Object*
       *  
       * @param  {Number} facilityId        Id Facility (Store)
       * @param  {Number} categoryTypeId    Tipologia di Categoria
       * @param  {Number} productCategoryId Id Prodotto
       * @param  {String}   from              Data da
       * @param  {String}   to                Data a
       * @param  {Array}  people            Lista di persone ( ageRange ) con espressa la quantity  
       * @param  {Number} dogs              Numero di cani
       * @param  {Number} cultureId         Id cultura
       * @return {ReservationCriteriaObject}           
       */
        function getReservationCriteria(facilityId, categoryTypeId, productCategoryId, from, to, people, dogs, placingId, vehicles, cultureId) {
            if (!cultureId) {
                cultureId = defaultCultureId;
            }
            // creo il periodo di ricerca from/to
            var period = getPeriod(from, to);
  
            // creo il criterio della risorsa
            var reservationUnitCriteriaExpression = getReservationUnitCriteriaExpression(period, productCategoryId, dogs, placingId);
  
            // creo i criteri sulle fasce di età
            var reservationUnitCriteriaPersons = [];
            angular.forEach(people, function(item) {
                reservationUnitCriteriaPersons.push(getReservationUnitCriteriaPerson(item.ageRangeId, period, item.quantity));
            });
  
            // creo i criteri sui veicoli
            var reservationUnitVehicles = [];
            angular.forEach(vehicles, function(item) {
                reservationUnitVehicles.push(getReservationUnitCriteriaVehicle(item.vehicleTypeId, period, item.quantity));
            });
            // se è stato già selezionato come sistemazione il camper, lo aggiungo come veicolo
            if(placingId && placingId == 1){
                var vehicle = _.find(reservationUnitVehicles, { vehicleTypeId: "3" });
                if(vehicle){
                    vehicle.quantity = "1";
                }                
            }
  
            var reservationUnitCriterias = {
                reservationFrom: from,
                reservationTo: to,
                reservationUnitPersons: reservationUnitCriteriaPersons,
                reservationUnitCriteriaExpressions: [reservationUnitCriteriaExpression],
                reservationUnitVehicles: reservationUnitVehicles
            };
  
            return {
                facilityId: facilityId,
                ownerClientId: 0,
                categoryTypeId: categoryTypeId,
                reservationUnitCriterias: [reservationUnitCriterias],
                cultureId: cultureId
            };
        }
  
        /**
       * *PeriodObject*
       *
       *  usato per determinare il tempo della vacanza
       * 
       * @param  {String} from da
       * @param  {String} to   a
       * @return {PeriodObject}      
       */
  
        function getPeriod(from, to) {
            return {
                isDefault: true,
                from: from,
                to: to
            };
        }
  
        /**
       * *ReservationUnitCriteriaExpressionObject*
       *
       * Criterio sulla risorsa
       * 
       * @param  {PeriodObject} period      periodo di ricerca
       * @param  {Number} productCategoryId Prodotto della categoria da ricercare
       * @param  {Number} dogs              Numeri di cani presenti nel criterio
       * @return {reservationUnitCriteriaExpressionObject}                   
       */
        function getReservationUnitCriteriaExpression(period, productCategoryId, dogs, placingId) {
            return {
                productCategoryId: productCategoryId,
                productId: "",
                localizedProductName: "",
                periods: [period],
                logicalOperator: "AND",
                noOfDogs: dogs,
                placingId: placingId ? placingId.toString() : null
            };
        }
  
        /**
       * *ReservationUnitCriteriaPersonObject*
       *
       * criterio applicato per fascia di età
       * 
       * @param  {Number} ageRangeId Id Fascia età  
       * @param  {PeriodObject} period     periodo di permanenza
       * @param  {Number} quantity  Numero di persone del criterio
       * @return {ReservationUnitCriteriaPersonObject}
       */
        function getReservationUnitCriteriaPerson(ageRangeId, period, quantity) {
            return {
                ageRangeId: ageRangeId,
                census: [],
                quantity: quantity,
                periods: [period],
                clientId: null
            };
        }
  
        function getReservationUnitCriteriaVehicle(id, period, quantity){
            return {
                vehicleTypeId: id.toString(),
                quantity: quantity.toString(),
                periods:[
                   period
                ],
                enableEditing: false
            }
        }
  
        /**
       * Short-cut per recuperare gli stores della organizzazione
       * @param  {Number} organizationId id della organizzazione
       * @return {[type]}                [description]
       */
        function getFacilities(organizationId) {
            return getSetupInfo(organizationId, 1033).then(function(response) {
                return response.facilitySetupInfo;
            });
        }
  
        /**
       * calcola la percentuale di completamento 
       * di un utente
       * possono essere passate le date per il controllo del range di età
       * @param  {[type]} data [description]
       * @return {[type]}      [description]
       */
        function clientCompletion(data, isOwner, minDate, maxDate) {
            
            var type = isOwner === true ? 'owner' : isOwner === 'adult' ? 'adult' : 'guest';
            var complete = 0;
            var totals = 11;
            if (data) {
                if (type === 'owner') {
                  totals = 13;
                } else if(type === 'adult'){
                  totals = 12;
                }
                if (angular.isDefined(data.client.firstName) && data.client.firstName) {
                    complete++;
                }
                if (angular.isDefined(data.client.lastName) && data.client.lastName) {
                    complete++;
                }
                if (angular.isDefined(data.client.isMale) && data.client.isMale !== null) {
                    complete++;
                }
                if (angular.isDefined(data.client.birthDate) && data.client.birthDate) {
                    var birthDate = new Date(data.client.birthDate).getTime();
                    var minBirthdate = maxDate ? new Date(minDate).getTime() : new Date(1800, 0, 1).getTime();
                    var maxBirthdate = minDate ? new Date(maxDate).getTime() : new Date(2200, 0, 1).getTime();
                    if (birthDate >= minBirthdate && birthDate < maxBirthdate) {
                        complete++;
                    }
                }
                if (angular.isDefined(data.client.birthCountryId) && data.client.birthCountryId) {
                    complete++;
                }
                if ((angular.isDefined(data.client.birthPlaceEntityId) && data.client.birthPlaceEntityId) || (angular.isDefined(data.client.birthPlaceFreeText) && data.client.birthPlaceFreeText)) {
                    complete++;
                }
                if (angular.isDefined(data.client.residenceCountryId) && data.client.residenceCountryId) {
                    complete++;
                }
                if (
                    (angular.isDefined(data.client.residenceCityEntityId) && data.client.residenceCityEntityId) ||
                    (angular.isDefined(data.client.residenceCityFreeText) && data.client.residenceCityFreeText)
                ) {
                    complete++;
                }
                if (angular.isDefined(data.client.residenceAddress) && data.client.residenceAddress) {
                    complete++;
                }
                if (angular.isDefined(data.client.residenceZipCode) && data.client.residenceZipCode) {
                    complete++;
                }
                if (angular.isDefined(data.client.citizenshipCountryId) && data.client.citizenshipCountryId) {
                    complete++;
                }
                // il capo gruppo deve specificare come contatto una mail
                if (
                    type === 'owner' &&
                    angular.isDefined(data.contacts) &&
                    data.contacts &&
                    angular.isArray(data.contacts) &&
                    _.some(data.contacts, function(item) {
                        return item.contactTypeId === 4;
                    })
                ) {
                    complete++;
                }
                // controllo che i documenti siano stati completati
                if (type === 'owner' || type === 'adult') {
                    // controllo che ci sia almeno un documento
                    var documentsDeleted = 0;
                    var allDocuments = _.every(data.identityDocuments, function(item) {
                        // 13
                        if (item.isDeleted) {
                            documentsDeleted++;
                            return true;
                        }
  
                        return (
                            item.expiryDate &&
                            item.issueDate &&
                            isValidDate(item.expiryDate) &&
                            isValidDate(item.issueDate) &&
                            new Date(item.issueDate).getTime() < new Date(item.expiryDate).getTime() &&
                            item.identityDocumentTypeId &&
                            item.number &&
                            item.number !== "" &&
                            item.issueCountryId &&
                            item.identityDocumentIssuingAuthorityId
                        );
                    });
                    if (allDocuments && !_.isEmpty(data.identityDocuments) && documentsDeleted < data.identityDocuments.length) {
                        complete++;
                    }
                }
            }
            return {
                percentage: Math.round(complete / totals * 100),
                totalSteps: totals,
                completed: complete,
                isCompleted: totals === complete
            };
        }
  
        /**
       * Restituisce la prenotazione pronta per 
       * essere modificata
       * @param  {[type]} reservation [description]
       * @param  {[type]} cultureId   [description]
       * @return {[type]}             [description]
       */
        function parseReservation(reservation, ageRanges) {
            var crew;
            if (!reservation) {
                return false;
            }
            var owner = _.find(reservation.clients, function(client) {
                return (client.clientId = reservation.reservationDetails.ownerClientId);
            });
            crew = _(reservation.reservationDetails.reservationUnit[0].reservationUnitPersons).reduce(function(list, item) {
                var ageRange = _.find(ageRanges, { ageRangeId: item.ageRangeId });
                for (var i = 0; i < item.quantity; i++) {
                    var client = _.extend({}, item);
                    var censed = item.census.shift();
                    if (angular.isDefined(censed)) {
                        _.extend(client, censed);
                    }
                    // se ho un clientId recupero i dati dal reservation.clients
                    if (client.clientId > 0) {
                        _.extend(client, _.find(reservation.clients, { clientId: client.clientId }));
                    } else {
                        // setto di default il campo di pubblicità
                        // per i nuovi clienti
                        client.allowAdvertising = true;
                        // per i nuovi clienti precompilo alcune informazioni con le info del capogruppo
                        if (owner) {
                            client.citizenshipCountryId = owner.citizenshipCountryId;
                            client.residenceCountryId = owner.residenceCountryId;
                            client.residenceCountryId = owner.residenceCountryId;
                            client.residenceCityFreeText = owner.residenceCityFreeText;
                            client.residenceAddress = owner.residenceAddress;
                            client.residenceZipCode = owner.residenceZipCode;
                            client.nativeResidenceCityEntity = owner.nativeResidenceCityEntity;
                            client.localizedResidenceCityEntity = owner.localizedResidenceCityEntity;
                        }
                    }
                    // residenceCityEntityId deve essere messo a null anziche a 0
                    if (client.residenceCityEntityId === 0 || angular.isUndefined(client.residenceCityEntityId)) {
                        client.residenceCityEntityId = null;
                    }
                    // birthPlaceEntityId deve essere messo a null anziche a 0
                    if (client.birthPlaceEntityId === 0 || angular.isUndefined(client.birthPlaceEntityId)) {
                        client.birthPlaceEntityId = null;
                    }
                    // clientStoreId deve essere messo a come il clientStore della prenotazione
                    if (client.clientStoreId === 0 || !client.clientStoreId) {
                        client.clientStoreId = reservation.clientStoreId;
                    }
  
                    var reservationDate = new Date(reservation.from);
  
                    list.push({
                        ageRange: ageRange,
                        client: client,
                        contacts: client.contacts ? client.contacts : [],
                        identityDocuments: client.identityDocuments ? client.identityDocuments : [],
                        licencePlates: client.licencePlates ? client.licencePlates : [],
                        minBirthDate: new Date(reservationDate.getFullYear() - (ageRange.maxAge === null ? 110 : ageRange.maxAge + 1), reservationDate.getMonth(), reservationDate.getDate()),
                        maxBirthDate: new Date(reservationDate.getFullYear() - (ageRange.minAge || 0), reservationDate.getMonth(), reservationDate.getDate())
                    });
                }
                return list;
            }, []);
  
            // riordinemento persone
            crew = _.sortBy(crew, function(item) {
                if (item.client.clientId === reservation.reservationDetails.ownerClientId) {
                    return 100;
                } else if (item.ageRange.maxAge === null) {
                    return 90;
                } else if (item.clientId === 0) {
                    return item.ageRange.maxAge - 1;
                } else {
                    return item.ageRange.maxAge;
                }
            });
            return crew.reverse();
        }
  
        /**
       * Fa il parsing dei parametri di ricerca (querystring) 
       * per recuperare un oggetto di criterio di ricerca
       * @param  {Object}       params querystring di ricerca
       * @return {SearchObject}        Oggetto di ricerca
       */
        function decodeSearchCriteria(params) {
            if (!params.facilityId) {
                var deferred = $q.defer();
                deferred.resolve(false);
                return deferred.promise;
            }
            var facilityId = parseInt(params.facilityId, 10);
            var cultureId = params.cultureId;
            var from = params.from;
            var to = params.to;
            var dogs = params.dogs ? parseInt(params.dogs, 10) : 0;
            var categoryTypeId = parseInt(params.categoryTypeId, 10);
            var productCategoryId = params.productCategoryId || null;
            var organizationId = parseInt(params.organizationId, 10);
            if (productCategoryId) {
                productCategoryId = parseInt(productCategoryId);
            }
            return podcamp.getAgeRanges(facilityId, cultureId).then(function(response) {
                var ageRanges = _.map(response, function(ageRange) {
                    var match = _.find(unescape(params.people).split(";"), function(item) {
                        return parseInt(item.split("=")[0], 10) === ageRange.ageRangeId;
                    });
                    var quantity = match ? parseInt(match.split("=")[1], 10) : 0;
                    ageRange.quantity = quantity;
                    return ageRange;
                });
  
                var criteria = {
                    organizationId: organizationId,
                    facilityId: facilityId,
                    categoryTypeId: categoryTypeId,
                    productCategoryId: productCategoryId,
                    from: from,
                    to: to,
                    ageRanges: ageRanges,
                    dogs: dogs,
                    cultureId: cultureId
                };
  
                return criteria;
            });
        }
        /**
       * @param  {[type]} criteria [description]
       * @return {[type]}          [description]
       */
        function encodeSearchCriteria(criteria, asObject) {
            var people = _.reduce(
                criteria.ageRanges,
                function(query, item) {
                    query += item.ageRangeId + "=" + item.quantity + ";";
                    return query;
                },
                ""
            );
            if (asObject) {
                return {
                    organizationId: criteria.organizationId,
                    cultureId: criteria.cultureId || "",
                    facilityId: criteria.facilityId,
                    categoryTypeId: criteria.categoryTypeId,
                    from: $filter("date")(criteria.from, "yyyy-MM-dd"),
                    to: $filter("date")(criteria.to, "yyyy-MM-dd"),
                    people: people,
                    dogs: criteria.dogQuantity || "",
                    productCategoryId: criteria.productCategoryId || ""
                };
            } else {
                var querystring = "cultureId=" + (criteria.cultureId || "");
                querystring += "&organizationId=" + criteria.organizationId;
                querystring += "&facilityId=" + criteria.facilityId;
                querystring += "&categoryTypeId=" + criteria.categoryTypeId;
                querystring += "&from=" + $filter("date")(criteria.from, "yyyy-MM-dd");
                querystring += "&to=" + $filter("date")(criteria.to, "yyyy-MM-dd");
                querystring += "&people=" + people;
                querystring += "&dogs=" + (isNaN(criteria.dogQuantity) ? 0 : !criteria.dogQuantity ? 0 : vm.dogQuantity);
                querystring += "&productCategoryId=" + (criteria.productCategoryId || "");
                return querystring;
            }
        }
  
        function encodeAgeRanges(ageRanges) {
            var people = _.reduce(
                ageRanges,
                function(string, item) {
                    string += item.ageRangeId + "=" + item.quantity + ";";
                    return string;
                },
                ""
            );
            return people;
        }
  
        /**
       * Salva il precheckin
       * @param  {PreCheckin} preCheckin  Oggetto del precheckin       
       */
        function savePreCheckin(preCheckin) {
            var url = apiHost + "/v1/pre-checkin/new";
            return $http
                .post(
                    url,
                    {
                        preCheckin: preCheckin
                    },
                    config
                )
                .then(function(response) {
                    return response.data.preCheckinCode;
                });
        }
  
        /**
       * Esegue il retrieve del precheckin
       * @param  {PreCheckin} preCheckinCode  Oggetto del precheckin       
       */
        function getPreCheckin(preCheckinCode, cultureId) {
            var url = apiHost + "/v1/pre-checkin/" + preCheckinCode + "?cultureId=" + cultureId;
            return $http.get(url, config).then(function(response) {
                return response.data.preCheckin;
            });
        }
  
        function stripeAddCustomerPreCheckin(token, preCheckinId, preCheckinCode, cultureId, facilityId, cardHolderEmail, cardHolderName){
          var url = apiHost + "/v1/stripe/add-precheckin-customer";
          return $http.post(url, {
                  preCheckinId: preCheckinId,
                  preCheckinCode: preCheckinCode,
                  facilityId: facilityId,
                  token: token,
                  cardHolderEmail: cardHolderEmail,
                  cardHolderName: cardHolderName
              },
              config
          ).then(function(response) {
              return response.data;
          });
      }
  
        /**
       * controlla se una data è valida o meno
       * @param  {[type]}  date [description]
       * @return {Boolean}      [description]
       */
        function isValidDate(date) {
            if (Object.prototype.toString.call(date) === "[object String]") {
                date = new Date(date);
            }
            if (Object.prototype.toString.call(date) === "[object Date]") {
                // it is a date
                if (isNaN(date.getTime())) {
                    // d.valueOf() could also work
                    return false;
                } else {
                    return true;
                }
            } else {
                return false;
            }
        }
  
        /**
       * Registra il device / utente
       * @param  {[type]} deviceToken [description]
       * @param  {[type]} pass        [description]
       * @return {[type]}             [description]
       */
        function subscribePushNotification(appId, deviceToken, pass, cultureId) {
            var data = {
                appId: appId,
                deviceId: deviceToken,
                cultureId: cultureId || defaultCultureId
            };
            if (pass) {
                data.pass = pass;
            }
            var url = apiHost + "/v1/push-notification/subscribe";
            return $http.post(url, data, config).then(function(response) {
                return response.data;
            });
        }
        /**
       * Rimuove dalla registrazine il device
       * @param  {[type]} deviceToken [description]
       * @return {[type]} [description]
       */
        function unsubscribePushNotification(deviceToken) {
            var url = apiHost + "/v1/push-notification/unsubscribe?id=" + deviceToken;
            return $http.delete(url, config).then(function(response) {
                return response.data;
            });
        }
        /**
       * Recupera gli interessi di una specifica facility
       * @param  {[type]} facilityId [description]
       * @param  {[type]} cultureId  [description]
       * @return {[type]}            [description]
       */
        function getInterestsPushNotification(facilityId, cultureId) {
            var url = apiHost + "/v1/push-notification/interests?facilityId=" + facilityId + "&cultureId=" + (cultureId || defaultCultureId);
            return $http.get(url, config).then(function(response) {
                return response.data;
            });
        }
        /**
       * Associa al device gli interessi
       * @param  {[type]} deviceToken [description]
       * @param  {[type]} interests   [description]
       * @return {[type]}             [description]
       */
        function saveInterestsPushNotification(deviceToken, interests) {
            var data = {
                deviceId: deviceToken,
                interests: interests
            };
            var url = apiHost + "/v1/push-notification/interests";
            return $http.post(url, data, config).then(function(response) {
                return response.data;
            });
        }
        /**
       * Interessi del device
       * @param  {[type]} deviceToken [description]
       * @return {[type]}             [description]
       */
        function getUserInterestsPushNotification(deviceToken, cultureId) {
            var url = apiHost + "/v1/push-notification/device-interests?deviceId=" + deviceToken + "&cultureId=" + (cultureId || defaultCultureId);
            return $http.get(url, config).then(function(response) {
                return response.data;
            });
        }
  
        function updateTokenPushNotification(oldToken, newToken) {
            var data = {
                oldToken: oldToken,
                newToken: newToken
            };
            var url = apiHost + "/v1/push-notification/update-token";
            return $http.post(url, data, config).then(function(response) {
                return response.data;
            });
        }
  
        function getHistoryPushNotifications(deviceToken, cultureId) {
            var url = apiHost + "/v1/push-notification/device-push-history?deviceId=" + deviceToken + "&cultureId=" + (cultureId || defaultCultureId);
            return $http.get(url, config).then(function(response) {
                return response.data;
            });
        }
  
        function getStayByPass(passCode, cultureId) {
            var url = apiHost + "/v1/reservedarea/stay?passCode=" + passCode + "&cultureId=" + (cultureId || defaultCultureId);
            return $http.get(url, config).then(function(response) {
                return response.data;
            });
        }
  
        function getCategoriesForStay(facilityId, cultureId) {
            var url = apiHost + "/v1/reservedarea/categories?facilityId=" + facilityId + "&cultureId=" + (cultureId || defaultCultureId);
            return $http.get(url, config).then(function(response) {
                return response.data;
            });
        }
  
        function getProductsForStay(productCategoryId, facilityId, from, to, stayId, cultureId) {
            var url =
                apiHost +
                "/v1/reservedarea/products?productCategoryId=" +
                productCategoryId +
                "&facilityId=" +
                facilityId +
                "&from=" +
                from +
                "&to=" +
                to +
                "&stayId=" +
                stayId +
                "&cultureId=" +
                (cultureId || defaultCultureId);
            return $http.get(url, config).then(function(response) {
                return response.data;
            });
        }
  
        function updateProductStay(stayItemId, productCategoryId, productId, cultureId) {
            var url = apiHost + "/v1/reservedarea/stay";
            return $http
                .post(
                    url,
                    {
                        stayItemId: stayItemId,
                        productCategoryId: productCategoryId,
                        productId: productId,
                        cultureId: cultureId || defaultCultureId
                    },
                    config
                )
                .then(function(response) {
                    return response.data;
                });
        }
  
        /**
         * @TODO cambiare la logica per rislvere il problema che si verifica quando 
         * esistono multiple fasce di età per adulti( vedi camping 6-59 e 60+ )
         * recupera la id dell'adulto
         * usato per definire la quantità di default diversa da 0
         * viene considerato adulto quello con la fascia di età più alta
         * @return {Number} 
         */
        function getAdultAgeRange(ageRanges){
          var ageRange = ageRanges[0];
          angular.forEach(ageRanges, function(item){
            if(item.minAge <= 18 && (item.maxAge === null || item.maxAge >= 18)){
                ageRange = item;
            }
          });
          return ageRange;
        }
    }
  })();
  